## [6.8.5] - 2024-05-22
### Lib
- Update youboralib version to `6.8.30`

## [6.8.4] - 2022-09-14
### Modified
- Fix `getPlayerVersion()` method after Bitmovin SDK update. 
### Updated
- Update youboralib version to `6.8.22`
- Update Kotlin version to `1.8.10`
- Update Bitmovin SDK to `3.39.0`

## [6.8.3] - 2022-09-14
### Modified
- Set targetSdkVersion to 33.
### Updated
- Update lib version to `6.8.1`
- Update Bitmovin SDK to `3.24.1`
### Added
- Player version collected automatically.

## [6.8.2] - 2022-05-04
### Modified
- Set targetSdkVersion to 32.
### Updated
- Update lib version to `6.7.67`
- Update Bitmovin SDK to `3.12.0`
### Added
- Assure compatibility with Bitmovin SDKs from `3.7.0` to `3.12.0`
- Listen to `SourceEvent.Error` events. 
### Fixed
- Add seek event listeners in live content.
- Avoid unexpected buffer at the beginning of live content.

## [6.8.1] - 2022-01-13
### Added
- Bitrate, rendition, video codec and fps.

## [6.8.0] - 2021-05-26
### Added
- Bitmovin v3 support.

## [6.7.6] - 2021-11-18
### Fixed
- Buffer sent after seek.

## [6.7.5] - 2021-06-29
### Fixed
- Seek.

## [6.7.4] - 2021-05-05
### Modified
- Deployment platform moved from Bintray to JFrog.

## [6.7.3] - 2020-08-11
### Added
- Dropped frames.

## [6.7.2] - 2020-05-22
### Fixed
- Seek when content paused.

## [6.7.1] - 2020-03-04
### Updated
- Youboralib version.

## [6.7.0] - 2020-02-26
### Refactored
- Adapter to work with the new Youboralib version.

## [6.5.1] - 2019-10-29
### Replaced
- Buffer listener by monitorPlayhead.

## [6.5.0] - 2019-10-02
### Added
- Ads rework.

## [6.4.1] - 2019-05-10
### Fixed
- Now the joinTime won't be sent until the flag listening to OnTimeChangedListener() is enabled.

## [6.4.0] - 2019-05-10
### Fixed
- Now the joinTime event should be sent correctly event when the first playhead is bigger than 0.1 
on live content.

## [6.3.0] - 2019-03-11
### Added
- Youboralib upgraded to version `6.3.9`

## [6.0.1] - 2018-07-30
### Fixed
- Now getPosition() should work as expected.

## [6.0.0] - 2018-07-12
### Added
- Release version.
