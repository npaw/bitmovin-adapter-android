package com.npaw.youbora.lib6.bitmovin

import com.bitmovin.player.api.Player
import com.bitmovin.player.api.event.EventListener
import com.bitmovin.player.api.event.PlayerEvent

import com.npaw.youbora.lib6.YouboraLog

import com.npaw.youbora.lib6.adapter.AdAdapter

open class BitmovinAdAdapter(player: Player) : AdAdapter<Player>(player) {

    private var adPosition: String? = null
    private var adProvider: String? = null

    private val adStartEvent = EventListener<PlayerEvent.AdStarted> {
        adPosition = it.position
        adProvider = it.clientType.toString()
        fireResume()
        fireStart()
        fireJoin()
    }

    private val adClickedEvent = EventListener<PlayerEvent.AdClicked> {
        fireClick(it.clickThroughUrl)
    }

    private val adErrorEvent = EventListener<PlayerEvent.AdError> {
        fireFatalError(it.code.toString(), it.message)
    }

    private val adQuartileEvent = EventListener<PlayerEvent.AdQuartile> {
        when (it.quartile.percentage) {
            0.25 -> fireQuartile(1)
            0.5 -> fireQuartile(2)
            0.75 -> fireQuartile(3)
        }
    }

    private val adSkippedEvent = EventListener<PlayerEvent.AdSkipped> { fireSkip() }
    private val adFinishedEvent = EventListener<PlayerEvent.AdFinished> { fireStop() }
    private val adBreakFinishedEvent = EventListener<PlayerEvent.AdBreakFinished> {
        fireAdBreakStop()
    }

    init { registerListeners() }

    override fun registerListeners() {
        super.registerListeners()

        player?.on(PlayerEvent.AdStarted::class.java, adStartEvent)
        player?.on(PlayerEvent.AdClicked::class.java, adClickedEvent)
        player?.on(PlayerEvent.AdError::class.java, adErrorEvent)
        player?.on(PlayerEvent.AdQuartile::class.java, adQuartileEvent)
        player?.on(PlayerEvent.AdSkipped::class.java, adSkippedEvent)
        player?.on(PlayerEvent.AdFinished::class.java, adFinishedEvent)
        player?.on(PlayerEvent.AdBreakFinished::class.java, adBreakFinishedEvent)
    }

    override fun unregisterListeners() {
        player?.off(PlayerEvent.AdStarted::class.java, adStartEvent)
        player?.off(PlayerEvent.AdClicked::class.java, adClickedEvent)
        player?.off(PlayerEvent.AdError::class.java, adErrorEvent)
        player?.off(PlayerEvent.AdQuartile::class.java, adQuartileEvent)
        player?.off(PlayerEvent.AdSkipped::class.java, adSkippedEvent)
        player?.off(PlayerEvent.AdFinished::class.java, adFinishedEvent)
        player?.off(PlayerEvent.AdBreakFinished::class.java, adBreakFinishedEvent)

        super.unregisterListeners()
    }

    override fun getPosition(): AdPosition {
        var adPosition = AdPosition.UNKNOWN

        if (this.adPosition == "post") adPosition = AdPosition.POST

        return adPosition
    }

    override fun fireStop(params: MutableMap<String, String>) {
        super.fireStop(params)
        adPosition = null
    }

    override fun getPlayhead(): Double? { return player?.currentTime }
    override fun getAdProvider(): String? { return adProvider }
}
