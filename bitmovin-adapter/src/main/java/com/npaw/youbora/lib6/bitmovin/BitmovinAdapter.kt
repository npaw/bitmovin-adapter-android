package com.npaw.youbora.lib6.bitmovin

import com.bitmovin.player.api.Player
import com.bitmovin.player.api.event.EventListener
import com.bitmovin.player.api.event.PlayerEvent
import com.bitmovin.player.api.event.SourceEvent
import com.npaw.youbora.lib6.Timer
import com.npaw.youbora.lib6.YouboraLog
import com.npaw.youbora.lib6.YouboraUtil
import com.npaw.youbora.lib6.adapter.PlayerAdapter

open class BitmovinAdapter(player: Player) : PlayerAdapter<Player>(player) {

    private var pauseTimer: Timer? = null
    private var lastPlayhead: Double? = null
    private var timeShiftedPlayhead: Double? = null
    private var lastBitrate = super.getBitrate()
    private var lastFps = super.getFramesPerSecond()
    private var lastRendition = super.getRendition()
    private var lastVideoCodec = super.getVideoCodec()

    private val playEvent = EventListener<PlayerEvent.Play> {
        pauseTimer?.stop()
        if (!player.isAd) fireStart()
    }
    private val playingEvent = EventListener<PlayerEvent.Playing> {
        YouboraLog.debug("bitmovin-playingEvent")
        fireSeekEnd()
        fireResume()
        if (getIsLive() == false) fireJoin()
    }

    private val pausedEvent = EventListener<PlayerEvent.Paused> { if (!player.isAd) pauseTimer?.start() }
    private val timeShiftEvent = EventListener<PlayerEvent.TimeShift> { fireSeekBegin() }
    private val seekEvent = EventListener<PlayerEvent.Seek> { fireSeekBegin() }
    private val playbackFinishedEvent = EventListener<PlayerEvent.PlaybackFinished> { fireStop() }
    private val timeShiftedEvent = EventListener<PlayerEvent.TimeShifted> {
        fireSeekEnd()
        timeShiftedPlayhead = getPlayhead()
    }
    private val stallStartedEvent = EventListener<PlayerEvent.StallStarted> {
        YouboraLog.debug("bitmovin-stallStartedEvent")
        if (!player.isAd) fireStart()

        timeShiftedPlayhead?.let {
            if (it != getPlayhead()){
                fireBufferBegin()
                timeShiftedPlayhead = null
            }
        } ?: kotlin.run { fireBufferBegin() }
    }
    private val stallEndedEvent = EventListener<PlayerEvent.StallEnded> {
        YouboraLog.debug("bitmovin-stallEndedEvent")
        fireSeekEnd()
        fireBufferEnd()
        if (getIsLive() == true) fireJoin()
    }

    private val errorEvent = EventListener<PlayerEvent.Error> {
        fireFatalError(it.code.toString(), it.message)
    }
    private val sourceErrorEvent = EventListener<SourceEvent.Error> {
        fireFatalError(it.code.toString(), it.message)
    }

    private val videoPlaybackQualityChangedEvent = EventListener<PlayerEvent.VideoPlaybackQualityChanged> {
        it.newVideoQuality?.let { videoQuality ->
            val bitrate = videoQuality.bitrate

            lastBitrate = bitrate.toLong()
            lastFps = videoQuality.frameRate.toDouble()
            lastVideoCodec = videoQuality.codec
            lastRendition = YouboraUtil.buildRenditionString(videoQuality.width, videoQuality.height, bitrate.toDouble())
        }
    }

    init { registerListeners() }

    override fun registerListeners() {
        super.registerListeners()
        pauseTimer = createPauseTimer()

        player?.on(PlayerEvent.Play::class.java, playEvent)
        player?.on(PlayerEvent.Playing::class.java, playingEvent)
        player?.on(PlayerEvent.Paused::class.java, pausedEvent)
        player?.on(PlayerEvent.Seek::class.java, seekEvent)
        player?.on(PlayerEvent.TimeShift::class.java, timeShiftEvent)
        player?.on(PlayerEvent.TimeShifted::class.java, timeShiftedEvent)
        player?.on(PlayerEvent.PlaybackFinished::class.java, playbackFinishedEvent)
        player?.on(PlayerEvent.StallStarted::class.java, stallStartedEvent)
        player?.on(PlayerEvent.StallEnded::class.java, stallEndedEvent)
        player?.on(PlayerEvent.Error::class.java, errorEvent)
        player?.on(SourceEvent.Error::class.java, sourceErrorEvent)
        player?.on(PlayerEvent.VideoPlaybackQualityChanged::class.java, videoPlaybackQualityChangedEvent)
    }

    override fun unregisterListeners() {
        pauseTimer = null

        player?.off(PlayerEvent.Play::class.java, playEvent)
        player?.off(PlayerEvent.Playing::class.java, playingEvent)
        player?.off(PlayerEvent.Paused::class.java, pausedEvent)
        player?.off(PlayerEvent.Seek::class.java, seekEvent)
        player?.off(PlayerEvent.TimeShift::class.java, timeShiftEvent)
        player?.off(PlayerEvent.TimeShifted::class.java, timeShiftedEvent)
        player?.off(PlayerEvent.PlaybackFinished::class.java, playbackFinishedEvent)
        player?.off(PlayerEvent.StallStarted::class.java, stallStartedEvent)
        player?.off(PlayerEvent.StallEnded::class.java, stallEndedEvent)
        player?.off(PlayerEvent.Error::class.java, errorEvent)
        player?.off(SourceEvent.Error::class.java, sourceErrorEvent)
        player?.off(PlayerEvent.VideoPlaybackQualityChanged::class.java, videoPlaybackQualityChangedEvent)

        super.unregisterListeners()
    }

    private fun createPauseTimer(): Timer {
        return Timer(object : Timer.TimerEventListener {
            override fun onTimerEvent(delta: Long) {
                firePause()
                pauseTimer?.stop()
            }
        }, 150)
    }

    override fun getPlayrate(): Double {
        return if (flags.isPaused)
            return super.getPlayrate()
        else
            player?.playbackSpeed?.toDouble() ?: super.getPlayrate()
    }

    override fun getPlayhead(): Double? {
        plugin?.isAdBreakStarted?.takeIf { !it }?.let { lastPlayhead = player?.currentTime }
        return lastPlayhead
    }

    override fun getDuration(): Double? { return player?.duration }
    override fun getIsLive(): Boolean? { return player?.isLive }
    override fun getResource(): String? { return player?.source?.config?.url }
    override fun getPlayerName(): String { return "Bitmovin" }
    override fun getDroppedFrames(): Int? { return player?.droppedVideoFrames }
    override fun getRendition(): String? { return lastRendition }
    override fun getVideoCodec(): String? { return lastVideoCodec }
    override fun getBitrate(): Long? { return lastBitrate }
    override fun getFramesPerSecond(): Double? { return lastFps }
    override fun getVersion(): String { return BuildConfig.VERSION_NAME + "-" + getPlayerName() }
    override fun getPlayerVersion(): String? { return Player.sdkVersion }

    override fun fireSeekBegin(convertFromBuffer: Boolean, params: MutableMap<String, String>) {
        pauseTimer?.stop()
        super.fireSeekBegin(convertFromBuffer, params)
    }

    override fun fireStop(params: MutableMap<String, String>) {
        super.fireStop(params)
        lastPlayhead = null
        timeShiftedPlayhead = null
        lastRendition = super.getRendition()
        lastVideoCodec = super.getVideoCodec()
        lastBitrate = super.getBitrate()
        lastFps = super.getFramesPerSecond()
    }
}
