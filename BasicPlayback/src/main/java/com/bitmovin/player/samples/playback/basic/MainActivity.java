/*
 * Bitmovin Player Android SDK
 * Copyright (C) 2017, Bitmovin GmbH, All Rights Reserved
 *
 * This source code and its use and distribution, is subject to the terms
 * and conditions of the applicable license agreement.
 */

package com.bitmovin.player.samples.playback.basic;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

import com.bitmovin.player.PlayerView;
import com.bitmovin.player.api.Player;
import com.bitmovin.player.api.source.SourceConfig;

import com.npaw.youbora.lib6.YouboraLog;
import com.npaw.youbora.lib6.bitmovin.BitmovinAdapter;
import com.npaw.youbora.lib6.plugin.Options;
import com.npaw.youbora.lib6.plugin.Plugin;

public class MainActivity extends AppCompatActivity {
    private PlayerView playerView;
    private Player player;

    private Plugin youboraPlugin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        playerView = findViewById(R.id.bitmovinPlayerView);

        initializePlayer();
        initializeYoubora();
    }

    private void initializeYoubora() {
        YouboraLog.setDebugLevel(YouboraLog.Level.VERBOSE);

        Options youboraOptions = new Options() {{
            setAccountCode("powerdev");
            setContentTitle("Test Content");
        }};

        youboraPlugin = new Plugin(youboraOptions, getApplicationContext());
        youboraPlugin.setActivity(this);
        youboraPlugin.setAdapter(new BitmovinAdapter(player));
    }

    @Override
    protected void onStart() {
        playerView.onStart();
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        playerView.onResume();
    }

    @Override
    protected void onPause() {
        playerView.onPause();
        super.onPause();
    }

    @Override
    protected void onStop() {
        playerView.onStop();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        playerView.onDestroy();
        super.onDestroy();
    }

    protected void initializePlayer() {
        player = Player.create(this);
        playerView.setPlayer(player);

        // load source using a source config

        // VOD DASH
        player.load(SourceConfig.fromUrl("https://bitdash-a.akamaihd.net/content/sintel/sintel.mpd"));

        // Live HLS
        // player.load(SourceConfig.fromUrl("https://cph-msl.akamaized.net/hls/live/2000341/test/master.m3u8"));
    }
}
